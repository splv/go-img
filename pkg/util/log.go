package util

import (
	"io"
	"log"
	"os"
	"time"
)
var (
	Info *log.Logger
	Warning *log.Logger
	Error * log.Logger
)

func init(){
	fileName := "./log/runtime_" + time.Now().Format("20180102") + ".txt"
	errFile,err:=os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0766)
	if err!=nil{
		panic(err)
	}

	Info = log.New(errFile,"Info:",log.Ldate | log.Ltime | log.Lshortfile)
	Warning = log.New(errFile,"Warning:",log.Ldate | log.Ltime | log.Lshortfile)
	Error = log.New(io.MultiWriter(os.Stderr,errFile),"Error:",log.Ldate | log.Ltime | log.Lshortfile)

}
