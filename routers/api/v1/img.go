package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
	"go-img/models"
	"go-img/pkg/e"
	"net/http"
)

// @Summary 获取图片列表
// @Description 通过page和size参数定义页数和每页个数
// @Accept  json
// @Produce  json
// @Param   page     query    int     true        "Page"
// @Param   size     query    int     true        "Size"
// @Success 200 {object} string {"code":200,"data":{},"msg":"ok",}
// @Router /api/v1/list [get]
func GetList(c *gin.Context) {
	var page int = 1
	if arg := c.Query("page"); arg != "" {
		page = com.StrTo(arg).MustInt()
	}
	var size int = 20
	if arg := c.Query("size"); arg != "" {
		size = com.StrTo(arg).MustInt()
	}
	data := models.GetList(page, size)
	code := e.InvalidParams
	if len(data) == 0 {
		code = e.ERROR
	} else {
		code = e.SUCCESS
	}
	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  e.GetMsg(code),
		"data": data,
	})

}

// @Summary 获取搜索关键字列表
// @Description 通过page和size参数定义页数和每页个数,查找keywords相关的图片
// @Accept  json
// @Produce  json
// @Param   page     query    int     true        "Page"
// @Param   size     query    int     true        "Size"
// @Param   keywords     query    string     true        "keywords"
// @Success 200 {object} string {"code":200,"data":{},"msg":"ok",}
// @Router /api/v1/search [get]
func GetSearch(c *gin.Context) {
	var page int = 1
	if arg := c.Query("page"); arg != "" {
		page = com.StrTo(arg).MustInt()
	}
	var size int = 20
	if arg := c.Query("size"); arg != "" {
		size = com.StrTo(arg).MustInt()
	}
	var keywords string = ""
	if arg := c.Query("keywords"); arg != "" {
		keywords = arg
	}
	data := models.GetSearch(keywords, page, size)
	code := e.InvalidParams
	if len(data) == 0 {
		code = e.ERROR
	} else {
		code = e.SUCCESS
	}
	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  e.GetMsg(code),
		"data": data,
	})
}
