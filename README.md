# go-img

#### 介绍
Golang写的爬虫系统，支持分页，自定义每页个数,支持瀑布流图片加载的网站

#### 软件架构
对外提供api接口访问获取爬虫爬取的数据

功能列表
* 分页功能
* 搜索功能

待扩展功能
* 支持换源
* 支持所有图片网站爬取
#### 安装教程
git clone 或下载zip
在项目根目录运行git mod download 下载项目所需依赖
执行main.go 运行程序

#### 目录结构
```text
├─conf 项目配置文件app.ini
├─log   日志存储位置
├─models 爬虫代码目录
├─pkg    工具类文件
│  ├─e   错误码及错误信息模块
│  ├─setting    配置文件调用
│  └─util       工具类
└─routers       路由模块
    └─api
        └─v1
```
#### Api接口
目前只有两个

api/v1/list   获取列表接口

可附带参数

page 页数

size 每页显示个数  最大为30

api/v1/search    搜索功能

可附带参数

page 页数

size 每页显示个数 最大为30

keywords   搜索关键字
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 声明
本项目仅作为参考学习使用